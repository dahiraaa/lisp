LispGame.lisp
- This file contains the functions that toggles
- prints put the board
- and checks if there are any zeroes in the board

Iteration: This iteration prints, toggles, and checks for zeroes in the lights out game. The functions can be tested using LispWorks by loading in the lisp file. 

Iteration 2: creates a random board, takes in user inputs and reads out results to the screen.

Itertion 3: This itearation was allowing the ai to play the game. The ai trys to beat the lights out game and in as much moves as it can.

Summary: This is a lights out game. This game allows users to toggle a postion on a board and the lights will go out of surrounding lights. In order to win the game, turn all lights out. The user is able to play, or the ai can play buy simply starting up start-ai function.

Design: The game is composed of many functions that work together to determine the steps to play. There are 8 total functions. 

How-to-play: In order to play, have LsipWorks downloaded. Load up the lisp file. To start the game, call the start-game function, which will allow the user to play the lights out game. The ai will use the chase the lights method to try to beat the game. After chasing the lights down to the last row, the ai will determine what to do depending what lights are on in the last row. It will repeat the same process until all the lights are out. To simply play, start-ai will jump start the game.
