;;LispGame.lisp
;;Abdirizak Dahir

;;toggle one light on the board
(defun toggle (board pos)
	(if (= 0 pos) ;;position of the board, checking if it equals 0
		(if (zerop (car board)) ;;
		(cons 1 (cdr board)) ;;swapping the board
		(cons 0 (cdr board)) ;;swapping the board
	)
		;;puts together the board
		(cons (car board) (toggle (cdr board) (- pos 1)))
	)
)

;;will print out the board
(defun print-board (board)
	(if (= 0 (mod (length board) 5))
		(format t "~% ~a" (car board))
		(format t " ~a" (car board))
	
	)
	;;is the rest of the board null
	(if (null (rest board))
		nil
		;;if not null, call the cdr/rest of the board
		(print-board (rest board))
	
	
	)	
)

;;are there zeroes in the board
;;determines if all the lights are out
;;returns true if there are no zeroes
(defun check-zero-p (alist)
	;;is the list empty
	(if (null alist)
		T
		;;current element
		(if (zerop (car alist))	
			(check-zero-p (cdr alist))
			nil
		)
	)
)


;;Creation of a random board 
;;with at least one light on.
(defun rand-board (num board)
	(if (> num 0)
		;;board is empty, calling a random number
		(rand-board (- num 1) (cons (random 2) board))
		;;one light on, being pushed to the board otherwise
		(cons 1 board)
	
	)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Game Functions;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;function to takes in a board and a postion to toggle
;;
(defun toggle-cases (board position)
	;;toggle the top edges of the board, also the bottom ones
	(if (or (or (= 1 position) (= 2 position)) (= 3 position))
	  (toggle (toggle (toggle (toggle board position) (- position 1)) (+ position 1)) (+ position 5))
	  (if (or (or (= 21 position) (= 22 position)) (= 23 position))
	    (toggle (toggle (toggle (toggle board position) (- position 1)) (+ position 1)) (- position 5))
	    ;;end of case 1
	    
	    ;;toggle the sides
	    (if (or (or (= 5 position) (= 10 position)) (= 15 position))
	      (toggle (toggle (toggle (toggle board position) (+ position 1)) (+ position 5)) (- position 5))
	      (if (or (or (= 9 position) (= 14 position)) (= 19 position))
	        (toggle (toggle (toggle (toggle board position) (- position 1)) (- position 5)) (+ position 5))
	      ;;end of case 2
	      
	      	;;corners of the board
	        (if (= 0 position)
	          (toggle (toggle (toggle board 0) 1) 5)
	            (if (= 20 position)
	              (toggle (toggle (toggle board 20) 21) 15)
	              (if (= 4 position)
	                (toggle (toggle (toggle board 4) 3) 9)
	                (if (= 24 position)
	                  (toggle (toggle (toggle board 24) 23) 19)
	                  (toggle (toggle (toggle (toggle (toggle board position) (- position 1)) (+ position 1)) (+ position 5)) (- position 5))
	              
	                   )
	                 )
	      
	               )
	             )
	           )
           )
        )
    )
)


;;let the user enter a number
(defun user-numb (prompt)
	(format T prompt)
	;;variable userNumb is the number user inputed
	(let ((hold-input (read)))
		(if (or (< hold-input 0) (> hold-input 24))
			(user-numb "Wrong range, has to between 0 and 24.")
			hold-input
		)
	)	
)

;;function to set  up the game
;;takes in a board, message and count
(defun allow-play (board message count)
	(let (
		(try (toggle-cases board (user-numb message)))
	)
	;;show the user the board
	(print-board try)
	;;otherwise the user is playing the game correctly
	(if (check-zero-p try)
		(format T "Score is " count)
		(allow-play try "some lights are still on. Play Again. " (+ 1 count))
		)
	)

)

;;function that starts the game up
(defun start-game()
	(let (
	(my-board (rand-board 24 nil))
	)
	(print-board my-board)
	(format T "turn all the lights off")
	(allow-play my-board "Try Turning the lights off " 0)
	
	)
)

;;checks if a light is on or not
(defun check-on (board pos)
	(if (zerop pos)
		(if (zerop (car board))
		0 ;; no the lights is off
		1 ;; light is on
		)
		;;recall the board again
		(check-on (cdr board) (- pos 1))
	
	)
)

;;function for the ai to choose a position
;;positioning of the ai compared to where it was the last time
(defun ai-pick (board pos)
	(let (
		;;accessing the board
		(selected (check-on board (- pos 5)))
	
		)
		(if (= 1 selected)
			pos
			(ai-pick board (+ pos 1))
		
		)
		
	
	)

)


;;rang the ai needs to operate in
(defun check-range (board range)
	;;checks if the location is zero
	(if (zerop (car board))
		
		(if (< range 1)
		
			22 
			(check-range (cdr board ) (- range 1))
		)
		;;happens if not all zeros
		1 
	)
)

;;function to run when the ai completes the game
;;basically when the ai reaches the last row
(defun end-ai (board)

	(if (= (check-on board 20) 1)
		20
		(if (= (check-on board 21) 1)
			21
			22
		)
	)
)

;;route to pick after coming to an end
(defun game-end (board count)
	(let (
		(route (end-ai board))
	
	)
	;;all the options available
	(if (= route 20)
		(ai-complete (toggle-cases (toggle-cases board 3) 4) (- count 2) 5)
		(if (= route 21)
			(ai-complete (toggle-cases (toggle-cases board 1) 4) (- count 2) 5)
			(ai-complete (toggle-cases board 3) (- count 1) 5)
			)
		)
	
	)

)

;;printing messages if the ai wins/loses
(defun ai-complete (board count start)
	(terpri)
	(format T "number of turns the ai took ~D" count)
	(terpri)
	;;uses toggle-cases to determine choices on to toggle on the board
   (let (
   		 	(my-ai (toggle-cases board (ai-pick board start)))
   		)
   		(print-board my-ai)
   
   		(if (check-zero-p my-ai)
   			(format T "AI Beat the game in: " count)
   			(if (zerop count)
   				(format T "AI failed")
   				(if (= (check-range my-ai 19) 22)
   					(ai-complete my-ai (- count 1) 5)
   				;;out of range
   					(ai-complete my-ai (- count 1) (+ start 1))
   				)
   			)
   		
   		)
   
   	)
)	

;;to start up the game
(defun start-ai()
	(let (
		(my-board (rand-board 25 nil))
	
	)
	;;print the board
	(print-board my-board)
	(ai-complete my-board 50 5)
	
	
	)
)